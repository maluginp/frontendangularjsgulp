var gulp      = require('gulp')
  , $         = require('gulp-load-plugins')()
  , config    = require('./config')
  , source    = require('vinyl-source-stream')
  , watchify  = require('watchify')
;

// Functions

// Copy from {path.fonts} to {path.dest}/assets/fonts
function copy_fonts(fromDir, toDir, cb) {
  gulp.src(fromDir)
    .pipe(gulp.dest(toDir))
    .on('end', cb || function() {})
    .on('error', $.util.log);
}

// Copy images from {path.images} to {path.dest}/images
function copy_images(fromDir, toDir, cb) {
  gulp.src(fromDir)
    .pipe($.imagemin())  // minifying image
    .pipe($.size({ showFiles: true }))
    .pipe(gulp.dest(toDir))
    .on('end', cb || function() {})
    .on('error', $.util.log);
}

// \note for manipulate stream using $.streamify

function compile_vendors(vendors, filename, toDir, cb) {
  $.util.log('Compiling vendors:', vendors, 'to:', toDir+"/"+filename);

  gulp.src(vendors)
    .pipe($.concat(filename))
    .pipe($.streamify($.uglify({ mangle: false })))
    // .pipe($.streamify($.rev()))
    .pipe($.size({showFiles:true}))
    .pipe(gulp.dest(toDir))
    .on('end', cb || function() {})
    .on('error', $.util.log);
}

function compile_templates(templates, rootName, moduleName, filename, toDir, cb) {
  $.util.log('Compiling templates:', templates, 'to:', toDir+"/"+filename);
  gulp.src(templates)
    .pipe($.angularTemplatecache(filename, {
      root:   rootName,
      module: moduleName
    }))
    // .pipe($.streamify($.rev()))
    .pipe(gulp.dest(toDir))
    .on('end', cb || function() {})
    .on('error', $.util.log);
}

function compile_styles(mainScss, includePaths, toDir, cb) {
  $.util.log('Compiling Scss style:', mainScss, 'to dir:', toDir);
  gulp.src(mainScss)
    .pipe($.plumber())
    .pipe($.sass({
      includePaths: includePaths,
      sourceComments: 'map'
    }))
    .pipe($.minifyCss())
    // .pipe($.streamify($.rev()))
    .pipe($.size({ showFiles: true }))
    .pipe(gulp.dest(toDir))
    .on('end', cb || function() {})
    .on('error', $.util.log);
}

function clean_dirs(dirs, cb) {
  $.util.log('Cleaing directories:', dirs);
  gulp.src(dirs, {read: false})
    .pipe($.rimraf({force: true}))
    .on('end', cb || function() {});
}


function compile_index(indexFile, filename, toDir, cb) {
  function inject(glob, path, tag) {
    return $.inject(
      gulp.src(glob, {
        cwd: path
      }), {
        starttag: '<!-- inject:' + tag + ':{{ext}} -->'
      }
    );
  }


    gulp.src(indexFile)
      .pipe(inject('./styles/app*.css', path, 'app-style'))
      .pipe(inject('./scripts/shim*.js', path, 'shim'))
      .pipe(inject('./scripts/vendor*.js', path, 'vendor'))
      .pipe(inject('./scripts/app*.js', path, 'app'))
      .pipe(inject('./scripts/templates*.js', path, 'templates'))
      .pipe(gulp.dest(path))
      .on('end', cb || function() {})
      .on('error', plugins.util.log);
  }
}

// Tasks
gulp.task('clean', function(){
  clean_dirs(config.path.dest);
});

gulp.task('compile-vendors', function(done){
  // vendors, filename, toDir, cb
  var vendors = require('./vendors')
  var dir = config.path.dest+config.vendors.dir;
  var filename = config.vendors.filename;

  compile_vendors(vendors, filename, dir, done);

});

gulp.task('compile-styles', function(done){
  // mainScss, includePaths, toDir, cb
  var mainScss = config.scss.main;
  var includePaths = config.scss.include;
  var dir = config.path.dest+config.scss.dir;

  compile_styles(mainScss, includePaths, dir, done);
});

gulp.task('compile-templates', function(done){
  compile_templates(
    config.templates.templates,
    config.templates.root,
    config.templates.module,
    config.templates.filename,
    config.path.dest+config.templates.dir,
    done
  );
});

// Using inject
gulp.task('compile-index',function(){
  
});

