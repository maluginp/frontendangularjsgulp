module.exports = {
  path : {
    dest: './public',
    dist: './dist',
  }, 
  vendors: {
    filename: 'vendors.js',
    dir: '/assets/js'
  },
  scss: {
    dir: '/assets/css',
    main: './app/styles/app.scss',
    include: ['app/bower_components']
  },
  templates: {
    templates: './app/views/**/*.html',
    root: '/views',
    module: 'App',
    filename: 'templates.js',
    dir: '/assets/js'
  }
}