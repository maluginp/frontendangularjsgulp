angular.module('App',['ngRoute']);

require('./controllers');
require('./directives');
require('./filters');
require('./helpers');
require('./routes');
require('./services');