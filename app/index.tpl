<!doctype html>
<html ng-app="App">
  <head>
    <meta charset="utf-8">
    <meta name="language" content="english">
    <title ng-bind="pageTitle + ' | App'">App</title>
    <!-- inject:app-style:css --><!-- endinject -->
  </head>
  <body>
    <header ui-view="header"></header>
    <main ui-view="content"></main>
    <footer ui-view="footer"></footer>
    <!--[if lt IE 9]><!-- inject:shim:js --><!-- endinject --><![endif]-->
    <!-- inject:vendor:js --><!-- endinject -->
    <!-- inject:app:js --><!-- endinject -->
    <!-- inject:templates:js --><!-- endinject -->
  </body>
</html>